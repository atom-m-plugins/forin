- Распаковать в папку: /plugins/
- Добавить в css:


```
#!CSS

.forin {
background: #FFF;
border-radius: 3px;
margin-bottom: 10px;
border: 1px solid #DDD;
overflow: hidden;
}
.forin a {
color: #555;
display:block;
text-decoration: none;
white-space: nowrap;
overflow: hidden;
text-overflow: ellipsis;
}
.forin a:before {
color: #777;
content: '›';
float:left;
font-size: 20px;
line-height: 26px;
margin: 0px 10px 0px 10px;
}
.forin a:hover:before {
color:#2BA2DF;
}
.forin .forinblock {
float:left;
width: 50%;
}
.forin .forinblock .forintitle {
background: #FFF;
background: linear-gradient(#FFF, #F5F5F5);
height: 35px;
color: #555;
border-right: 1px solid #DDD;
font: 300 12px/35px warhelioscondc,Helvetica,sans-serif;
padding: 0px 10px;
text-transform: uppercase;
}
.forin .forinblock:last-child .forintitle {
border-right:none;
}
.forin .forinblock:last-child .forincontent {
border-right:none;
}
.forin .forinblock .forincontent {
border-right: 1px solid #DDD;
}
.forin .forinblock .forincontent .matlist {
border-top: 1px solid #DDD;
background: #FFF;
height: 30px;
line-height: 30px;
overflow: hidden;
}
.forin .forinblock .forincontent .matlist:hover {
background: #F5F5F5;
box-shadow: inset 0px 0px 3px #DDD;
}
.forin .forinblock .forincontent .matlist .matdate {
background: #1EAABF;
color: #FFF;
float: right;
font: 600 12px/12px Arial,Helvetica,sans-serif;
padding: 4px 8px;
min-width: 20px;
text-align: center;
border-radius: 3px;
margin: 5px;
}
.forin .forinblock .forincontent .matlist .matlinks {
}
```

Добавить в шаблон метку: {{ forin }} - туда где необходимо вывести данный плагин.