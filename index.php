<?php
class forin {
    public function common($html) {
        $this->marker = '{{ forin }}';

        $limit = '6';
        $cache = '1800';
        $themesort = 'time';
        $postsort = 'posts';

        $forumthemes = fetch('forumThemes',false,array('limit'=>$limit,'sort'=>$themesort,'cache'=>$cache));
        foreach($forumthemes as $n => $theme) {
            $forumthemes[$n]['module']= 'forum';
            $forumthemes[$n]['url'] = get_url('/forum/view_theme/'.$theme['id'].'/');
        }

        $forumposts = fetch('forumThemes',false,array('limit'=>$limit,'sort'=>$postsort,'cache'=>$cache));
        foreach($forumposts as $n => $post) {
            $forumposts[$n]['module']= 'forum';
            $forumposts[$n]['url'] = get_url('/forum/view_theme/'.$post['id'].'/');
        }

        $Viewer = new Viewer_Manager;
        $template = file_get_contents(dirname(__FILE__).'/template/index.html');
        $template = $Viewer->parseTemplate($template, array('themes' => $forumthemes,'posts' => $forumposts));

        $html = str_replace($this->marker, $template, $html);

        return $html;
    }
}
?>